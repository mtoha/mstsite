﻿using MstSite.Common;
using MstSite.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace MstSite.Provider
{
    public class KodePosProvider
    {
        public static List<KodePosModel> Get()
        {
            List<KodePosModel> oResult = new List<KodePosModel>();
            SqlConnection conn = new SqlConnection(CommonConstant.ConnectionString);
            conn.Open();
            string sQuery = @"SELECT ID, Kelurahan, Kecamatan, Propinsi, NoKodePos, Jenis, Kabupaten
                            FROM dbo.KodePos;
                            
                                                                        ";
            SqlCommand cmd = new SqlCommand(sQuery, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                KodePosModel oItem = new KodePosModel();
                oItem.ID = UtilityClass.GetIntFromSQL(dr["ID"]);
                oItem.JenisWilayah = UtilityClass.GetStringFromSQL(dr["Jenis"]);
                oItem.Kecamatan = UtilityClass.GetStringFromSQL(dr["Kecamatan"]);
                oItem.Kelurahan = UtilityClass.GetStringFromSQL(dr["Kelurahan"]);
                oItem.Kode = UtilityClass.GetStringFromSQL(dr["NoKodePos"]);
                oItem.NamaKabupaten = UtilityClass.GetStringFromSQL(dr["Kabupaten"]);
                oItem.Propinsi = UtilityClass.GetStringFromSQL(dr["Propinsi"]);


                oResult.Add(oItem);

            }

            // It will be better if you close DataReader
            dr.Close();
            conn.Close();

            return oResult;
        }

        internal static List<PropinsiModel> GetPropinsi()
        {
            List<PropinsiModel> oResult = new List<PropinsiModel>();
            SqlConnection conn = new SqlConnection(CommonConstant.ConnectionString);
            conn.Open();
            string sQuery = @"SELECT distinct Propinsi
                            FROM dbo.KodePos;
                            
                                                                        ";
            SqlCommand cmd = new SqlCommand(sQuery, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                PropinsiModel oItem = new PropinsiModel();
                oItem.Name = UtilityClass.GetStringFromSQL(dr["Propinsi"]);


                oResult.Add(oItem);

            }

            // It will be better if you close DataReader
            dr.Close();
            conn.Close();

            return oResult;
        }

        internal static int Create(KodePosModel oInput)
        {
            int iResult = -99;
            string sQuery = string.Empty;
            try
            {
                sQuery = @"INSERT INTO dbo.KodePos
                            (Kelurahan, Kecamatan, Propinsi, NoKodePos, Jenis, Kabupaten)
                            VALUES({{Kelurahan}}, {{Kecamatan}}, {{Propinsi}}, {{NoKodePos}}, {{Jenis}}, {{Kabupaten}});select @@identity
; ";
                sQuery = sQuery.Replace("{{Kelurahan}}", UtilityClass.ValueSQL(oInput.Kelurahan));
                sQuery = sQuery.Replace("{{Kecamatan}}", UtilityClass.ValueSQL(oInput.Kecamatan));
                sQuery = sQuery.Replace("{{Propinsi}}", UtilityClass.ValueSQL(oInput.Propinsi));
                sQuery = sQuery.Replace("{{NoKodePos}}", UtilityClass.ValueSQL(oInput.Kode));
                sQuery = sQuery.Replace("{{Jenis}}", UtilityClass.ValueSQL(oInput.JenisWilayah));
                sQuery = sQuery.Replace("{{Kabupaten}}", UtilityClass.ValueSQL(oInput.NamaKabupaten));

                using (SqlConnection con = new SqlConnection(CommonConstant.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sQuery, con))
                    {

                        string getValue = cmd.ExecuteScalar().ToString();
                        iResult = Convert.ToInt32(getValue);
                    } // command disposed here
                } //connection closed and disposed here
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return iResult;
        }

        public static KodePosModel Get(int id)
        {
            KodePosModel oResult = new KodePosModel();
            SqlConnection conn = new SqlConnection(CommonConstant.ConnectionString);
            conn.Open();
            string sQuery = @"SELECT ID, Kelurahan, Kecamatan, Propinsi, NoKodePos, Jenis, Kabupaten
                            FROM dbo.KodePos Where ID = {{ID}}; 
                                                                        ";
            sQuery = sQuery.Replace("{{ID}}", UtilityClass.ValueSQL(id));

            SqlCommand cmd = new SqlCommand(sQuery, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                KodePosModel oItem = new KodePosModel();
                oItem.ID = UtilityClass.GetIntFromSQL(dr["ID"]);
                oItem.JenisWilayah = UtilityClass.GetStringFromSQL(dr["Jenis"]);
                oItem.Kecamatan = UtilityClass.GetStringFromSQL(dr["Kecamatan"]);
                oItem.Kelurahan = UtilityClass.GetStringFromSQL(dr["Kelurahan"]);
                oItem.Kode = UtilityClass.GetStringFromSQL(dr["NoKodePos"]);
                oItem.NamaKabupaten = UtilityClass.GetStringFromSQL(dr["Kabupaten"]);
                oItem.Propinsi = UtilityClass.GetStringFromSQL(dr["Propinsi"]);


                oResult = oItem;

            }

            // It will be better if you close DataReader
            dr.Close();
            conn.Close();

            return oResult;
        }

        internal static List<KodePosModel> Get(string sKeyword)
        {
            List<KodePosModel> oResult = new List<KodePosModel>();
            SqlConnection conn = new SqlConnection(CommonConstant.ConnectionString);
            conn.Open();
            string sQuery = @"SELECT ID, Kelurahan, Kecamatan, Propinsi, NoKodePos, Jenis, Kabupaten
                            FROM dbo.KodePos Where ('' = {{sKeyword}} or Kelurahan = {{sKeyword}} )
                                    or ('' = {{sKeyword}} or Kecamatan = {{sKeyword}} )
                                    or ('' = {{sKeyword}} or Propinsi = {{sKeyword}} )
                                    or ('' = {{sKeyword}} or NoKodePos = {{sKeyword}} )
                                    or ('' = {{sKeyword}} or Jenis = {{sKeyword}} )
                                    or ('' = {{sKeyword}} or Kabupaten = {{sKeyword}} )


                            ;";
            sQuery = sQuery.Replace("{{sKeyword}}", UtilityClass.ValueSQL(sKeyword));

            SqlCommand cmd = new SqlCommand(sQuery, conn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                KodePosModel oItem = new KodePosModel();
                oItem.ID = UtilityClass.GetIntFromSQL(dr["ID"]);
                oItem.JenisWilayah = UtilityClass.GetStringFromSQL(dr["Jenis"]);
                oItem.Kecamatan = UtilityClass.GetStringFromSQL(dr["Kecamatan"]);
                oItem.Kelurahan = UtilityClass.GetStringFromSQL(dr["Kelurahan"]);
                oItem.Kode = UtilityClass.GetStringFromSQL(dr["NoKodePos"]);
                oItem.NamaKabupaten = UtilityClass.GetStringFromSQL(dr["Kabupaten"]);
                oItem.Propinsi = UtilityClass.GetStringFromSQL(dr["Propinsi"]);


                oResult.Add(oItem);

            }

            // It will be better if you close DataReader
            dr.Close();
            conn.Close();

            return oResult;
        }

        internal static int Delete(int id)
        {
            int iResult = -99;
            string sQuery = string.Empty;
            try
            {
                sQuery = @"delete dbo.KodePos
                            WHERE ID={{ID}};
";
                sQuery = sQuery.Replace("{{ID}}", UtilityClass.ValueSQL(id));


                using (SqlConnection con = new SqlConnection(CommonConstant.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sQuery, con))
                    {
                        iResult = cmd.ExecuteNonQuery();
                    } // command disposed here
                } //connection closed and disposed here
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return iResult;
        }

        internal static int Update(KodePosModel oParam)
        {
            int iResult = -99;
            string sQuery = string.Empty;
            try
            {
                sQuery = @"UPDATE dbo.KodePos
                            SET Kelurahan={{Kelurahan}} , Kecamatan={{Kecamatan}}, Propinsi={{Propinsi}}, 
                                NoKodePos={{NoKodePos}}, Jenis={{Jenis}}, Kabupaten={{Kabupaten}}
                            WHERE ID={{ID}};
";
                sQuery = sQuery.Replace("{{Kelurahan}}", UtilityClass.ValueSQL(oParam.Kelurahan));
                sQuery = sQuery.Replace("{{Kecamatan}}", UtilityClass.ValueSQL(oParam.Kecamatan));
                sQuery = sQuery.Replace("{{Propinsi}}", UtilityClass.ValueSQL(oParam.Propinsi));
                sQuery = sQuery.Replace("{{NoKodePos}}", UtilityClass.ValueSQL(oParam.Kode));
                sQuery = sQuery.Replace("{{Jenis}}", UtilityClass.ValueSQL(oParam.JenisWilayah));
                sQuery = sQuery.Replace("{{Kabupaten}}", UtilityClass.ValueSQL(oParam.NamaKabupaten));
                sQuery = sQuery.Replace("{{ID}}", UtilityClass.ValueSQL(oParam.ID));


                using (SqlConnection con = new SqlConnection(CommonConstant.ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(sQuery, con))
                    {
                        iResult = cmd.ExecuteNonQuery();
                    } // command disposed here
                } //connection closed and disposed here
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return iResult;
        }
    }
}
