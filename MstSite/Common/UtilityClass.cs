﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Data;
using System.Data.OleDb;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace MstSite.Common
{


    public static class ExceptionExtensions
    {
        public static void SetMessage(this Exception exception, string message)
        {
            if (exception == null)
                throw new ArgumentNullException(nameof(exception));

            var type = typeof(Exception);
            var flags = BindingFlags.Instance | BindingFlags.NonPublic;
            var fieldInfo = type.GetField("_message", flags);
            fieldInfo.SetValue(exception, message);
        }
    }
    public class UtilityClass
    { 
        public static bool IsLeapYear(DateTime dtInput)
        {
            int year = dtInput.Year;
            if (year < 1 || year > 9999)
            {
                throw new ArgumentOutOfRangeException("year", "ArgumentOutOfRange_IsLeapYear");
            }
            return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
        }

        public static bool ByteArrayToFile(string fileName, byte[] byteArray)
        {
            try
            {
                using (var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                {
                    fs.Write(byteArray, 0, byteArray.Length);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Exception caught in process: " +  ex.ToString());
                return false;
            }
        }
        public static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
        public static DateTime ParseDate(string sDate)
        {
            DateTime result = DateTime.ParseExact(sDate, "yyyyMMdd", CultureInfo.InvariantCulture);
            return result;
        }
        public static bool IsAlphaWithSpace(string input)
        {
            return Regex.IsMatch(input, @"^[a-zA-Z\s]+$");
        }
        public static string Replace(string source, string chars, string replacement)
        {
            string sResult = source;
            while (sResult.Contains(chars))
            {
                sResult = sResult.Replace(chars, replacement);
            }
            return sResult;
        }
        public static string ReplaceSpace(string source)
        {
            string sResult = source;
            while (source.Contains("   "))
            {
                sResult = Replace(source, "   ", " ");
            }
            while (source.Contains("  "))
            {
                sResult = Replace(source, "  ", " ");
            }
            return sResult;
        }
        public static string ReplaceComma(string source)
        {
            string sResult = source;
            while (sResult.Contains(",,,"))
            {
                sResult = Replace(sResult, ",,,", ",");
            }
            while (sResult.Contains(",,"))
            {
                sResult = Replace(sResult, ",,", ",");
            }
            return sResult;
        }

        //SQL Tools
        public static string ValueSQL(string sValue)
        {
            string sAntiSQLInj = "";
            if (sValue == null)
                sAntiSQLInj = "";
            else
                sAntiSQLInj = sValue.Replace("'", "''");
            return "'" + sAntiSQLInj + "'";
        }

        public static string ValueSQL(double dValue)
        {
            string sAntiSQLInj = "";
            if (dValue == null)
                sAntiSQLInj = "0";
            else
                sAntiSQLInj = dValue.ToString();
            return "'" + sAntiSQLInj + "'";
        }

        public static string DecimalToString(decimal dValue)
        {
            return dValue.ToString("0,0.00", new CultureInfo("de-DE", false));

            //CultureInfo culture = CultureInfo.CreateSpecificCulture("de-DE");

            //Console.WriteLine(d.ToString("N3", culture)
        }

        public static string ValueSQLForLike(string sValue)
        {
            string sAntiSQLInj = "";
            if (sValue == null)
                sAntiSQLInj = "";
            else
                sAntiSQLInj = sValue.Replace("'", "''");
            return "'" + sAntiSQLInj + "%'";
        }
        public static string ValueSQL(int iValue)
        {
            string sAntiSQLInj = "";
            if (iValue == null)
                sAntiSQLInj = "NULL";
            else
                sAntiSQLInj = iValue.ToString();
            return sAntiSQLInj;
        }

        public static string ValueSQL(int? iValue)
        {
            string sAntiSQLInj = "";
            if (iValue == null)
                sAntiSQLInj = "NULL";
            else
                sAntiSQLInj = iValue.ToString();
            return sAntiSQLInj;
        }

        public static string ValueSQL(decimal dValue)
        {
            string sAntiSQLInj = "";
            if (dValue == null)
                sAntiSQLInj = "NULL";
            else
                sAntiSQLInj = dValue.ToString().Replace(",", ".");
            return sAntiSQLInj;
        }

        public static string ValueSQL(decimal? dValue)
        {
            string sAntiSQLInj = "";
            if (dValue == null)
                sAntiSQLInj = "NULL";
            else
                sAntiSQLInj = dValue.ToString().Replace(",", ".");
            return sAntiSQLInj;
        }

        public static string ValueSQL(DateTime? sValue)
        {
            string sAntiSQLInj = "";
            if (sValue == null)
                sAntiSQLInj = "";
            else
                sAntiSQLInj = Convert.ToDateTime(sValue).ToString("yyyyMMdd").Replace("'", "''");
            return "'" + sAntiSQLInj + "'";
        }
        public static string ValueSQL(DateTime sValue)
        {
            //In SQL Server : 
            //  Select convert(varchar(8),cast('12/24/2016' as date),112)
            //convert(varchar(8),columnName,112)
            string sAntiSQLInj = "";
            if (sValue == null)
                sAntiSQLInj = "";
            else
                sAntiSQLInj = sValue.ToString("yyyyMMdd").Replace("'", "''");
            return "'" + sAntiSQLInj + "'";
        }

        public static string ValueSQLYYYYMM(DateTime sValue)
        {
            //In SQL Server : 
            //  Select convert(varchar(8),cast('12/24/2016' as date),112)
            //convert(varchar(6),columnName,112)
            string sAntiSQLInj = "";
            if (sValue == null)
                sAntiSQLInj = "";
            else
                sAntiSQLInj = sValue.ToString("yyyyMM").Replace("'", "''");
            return "'" + sAntiSQLInj + "'";
        }
        public static string ValueSQLDateTime(DateTime sValue)
        {
            string sAntiSQLInj = "";
            if (sValue == null)
                sAntiSQLInj = "";
            else
                sAntiSQLInj = sValue.ToString("yyyyMMdd HH:mm:ss").Replace("'", "''");
            return "'" + sAntiSQLInj + "'";
        }

        public static string GetStringFromSQL(object oParam)
        {
            return oParam == DBNull.Value ? "" : Convert.ToString(oParam);
        }
        public static int GetIntFromSQL(object oParam)
        {
            return oParam == DBNull.Value ? 0 : Convert.ToInt32(oParam);
        }

        public static DateTime GetDateFromSQL(object oParam)
        {
            return oParam == DBNull.Value ? new DateTime(1900, 1, 1) : Convert.ToDateTime(oParam);
        }



        public static DateTime GetDateFromExcelMDDYYYYHHMMSSTT(object oParam)
        {
            var cdate = oParam == DBNull.Value ? "1/1/1900 00:00:00 AM" : oParam.ToString();
            var dt = DateTime.ParseExact(cdate, "M/d/yyyy hh:mm:ss tt", CultureInfo.CurrentCulture);
            return dt;
        }

        public static double GetDoubleFromSQL(object oParam)
        {
            return oParam == DBNull.Value ? 0.0 : Convert.ToDouble(oParam);
        }
        public static decimal GetDecimalFromSQL(object oParam)
        {
            return oParam == DBNull.Value ? 0 : Convert.ToDecimal(oParam);
        }
        public static decimal GetDecimalFromExcel(object oParam)
        {
            if (oParam == DBNull.Value)
                return 0;
            string sParam = oParam.ToString().ToLower();
            if (sParam.Contains("%"))
                sParam = sParam.Replace("%", "");
            if (sParam.ToLower().Contains("e"))
                return Decimal.Parse(sParam, System.Globalization.NumberStyles.Float);
            else
                return Convert.ToDecimal(sParam);
        }
        public static decimal GetDecimalOrPercentageFromExcel(object oParam)
        {
            if (oParam == DBNull.Value)
                return 0;
            string sParam = oParam.ToString().Replace("%", "");
            if (sParam.ToLower().Contains("e"))
                return Decimal.Parse(sParam, System.Globalization.NumberStyles.Float);
            else
                return Convert.ToDecimal(sParam);
        }


        //public static DataTable ReadExcelToTable(string path)
        //{
        //    //Connection String
        //    string connstring = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";

        //    using (OleDbConnection conn = new OleDbConnection(connstring))
        //    {
        //        conn.Open();
        //        //Get All Sheets Name
        //        DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });

        //        //Get the First Sheet Name
        //        string firstSheetName = sheetsName.Rows[0][2].ToString();

        //        //Query String 
        //        string sql = string.Format("SELECT * FROM [{0}]", firstSheetName);
        //        OleDbDataAdapter ada = new OleDbDataAdapter(sql, connstring);
        //        DataSet set = new DataSet();
        //        ada.Fill(set);
        //        return set.Tables[0];
        //    }
        //}
        //public static DataTable ReadExcel2016ToTable(string path)
        //{
        //    //Connection String
        //    string connstring = "Provider=Microsoft.ACE.OLEDB.16.0;Data Source=" + path + ";Extended Properties='Excel 8.0;HDR=NO;IMEX=1';";

        //    using (OleDbConnection conn = new OleDbConnection(connstring))
        //    {
        //        conn.Open();
        //        //Get All Sheets Name
        //        DataTable sheetsName = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "Table" });

        //        //Get the First Sheet Name
        //        string firstSheetName = sheetsName.Rows[0][2].ToString();

        //        //Query String 
        //        string sql = string.Format("SELECT * FROM [{0}]", firstSheetName);
        //        OleDbDataAdapter ada = new OleDbDataAdapter(sql, connstring);
        //        DataSet set = new DataSet();
        //        ada.Fill(set);
        //        return set.Tables[0];
        //    }
        //}

        public static bool IsNullID(int ID)
        {
            bool bIsNull = true;
            if (ID == null || ID <= 0)
                return bIsNull;
            else
                return false;

        }
    }

}
