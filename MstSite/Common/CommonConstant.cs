﻿using Microsoft.Extensions.Configuration;
using MstSite.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Mvc;

namespace MstSite.Common
{
    public class CommonConstant
    {
        public static string ConnectionString
        {
            get
            {
                var oResult = MySalesConfig.Get("ConnectionString");
                return oResult;
            }
        }

        public static string ConnectionStrings(string? sKode)
        {
            if (string.IsNullOrWhiteSpace(sKode))
                return MySalesConfig.GetConn("MySales");
            else
                return MySalesConfig.GetConn(sKode);
        }

    }
}
