﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MstSite.Models
{
    public class PropinsiModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}
