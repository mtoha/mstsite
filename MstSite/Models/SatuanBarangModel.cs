﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MstSite.Models
{
    public class SatuanBarangModel
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public string Lambang { get; set; }
        public string User_Create { get; set; }
        public DateTime Date_Create { get; set; }
    }
}
