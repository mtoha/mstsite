﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MstSite.Models
{
    public class KodePosModel
    {
        public int ID { get; set; }
        public string Kode { get; set; }
        public string Kelurahan { get; set; }
        public string Kecamatan { get; set; }
        public string JenisWilayah { get; set; }
        public string NamaKabupaten { get; set; }
        public string Propinsi { get; set; }

        public string NoKodePos
        {
            get
            {
                return Kode;
            }
        }
        public string Jenis
        {
            get
            {
                return JenisWilayah;
            }
        }
        public string Kabupaten
        {
            get
            {
                return NamaKabupaten;
            }
        }
    }
}
