const routes = [
    { path: "/home", component: home },
    { path: "/satuanbarang", component: satuanbarang },
    { path: "/kodepos", component: kodepos }
]

const router = new VueRouter({
    routes
})

const app = new Vue({
    router
}).$mount("#appMain")