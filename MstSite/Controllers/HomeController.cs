﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MstSite.Models;
using MstSite.Provider;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MstSite.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost("AsJson")]
        public async Task<JsonResult> AsJson()
        {
            List<KodePosModel> oResult = KodePosProvider.Get();

            return new JsonResult(new { data = oResult });
        }
    }
}
