﻿using AspNetCore.Reporting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MstSite.Common;
using MstSite.Models;
using MstSite.Provider;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MstSite.Controllers
{
    public class KodePosController : BaseController
    {
        // GET: KodePosController
        public ActionResult Index()
        {
            return View();
        }

        // GET: KodePosController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: KodePosController/Create
        public ActionResult Create()
        {
            if (CheckLogin() != null)
                return (ActionResult)CheckLogin();




            //System.Collections.Generic.IEnumerable<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem> oKecamatanList = KodePosProvider.GetPropinsi()
            //              .Select(a => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
            //              {
            //                  Value = a.Name,
            //                  Text = a.Name
            //              })
            //              ;
            //ViewBag.KecamatanList = oKecamatanList;

            System.Collections.Generic.IEnumerable<Microsoft.AspNetCore.Mvc.Rendering.SelectListItem> oPropinsiList = KodePosProvider.GetPropinsi()
                          .Select(a => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem()
                          {
                              Value = a.Name,
                              Text = a.Name
                          })
                          ;
            ViewBag.PropinsiList = oPropinsiList;
            return View();
        }

        // POST: KodePosController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("ID,Kode,Kelurahan,Kecamatan,JenisWilayah,NamaKabupaten,Propinsi")] KodePosModel oParam)
        {
            try
            {
                KodePosProvider.Create(oParam);

                TempData["JScript"] = "Data Saved " + oParam.Kode;
            }
            catch (Exception ex)
            {

                TempData["JScript"] = "failed to save" + ex.Message; 
                
                return Create();
            }
            return Redirect("~/Home");
        }

        // GET: KodePosController/Edit/5
        public ActionResult Edit(int id)
        {
            if (CheckLogin() != null)
                return (ActionResult)CheckLogin();

            KodePosModel oModel = KodePosProvider.Get(id);
            if (oModel == null || oModel.ID < 0)
            {
                TempData["JScript"] = "Data Not Found";
            }
            return View(oModel);
        }

        // POST: KodePosController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, [Bind("ID,Kode,Kelurahan,Kecamatan,JenisWilayah,NamaKabupaten,Propinsi")] KodePosModel oParam)
        {
            try
            {
                if (id != oParam.ID)
                {
                    return NotFound();
                }

                //Update to DB
                KodePosProvider.Update(oParam);

                TempData["JScript"] = "Data updated";
                return Redirect("~/Home");
            }
            catch
            {
                return View();
            }
        }

        // GET: KodePosController/Delete/5
        public ActionResult Delete(int id)
        {
            if (CheckLogin() != null)
                return (ActionResult)CheckLogin();


            KodePosModel oModel = KodePosProvider.Get(id);
            if (oModel == null || oModel.ID <= 0)
            {
                TempData["JScript"] = "Data Not Found";
            }
            return View(oModel);
        }

        // POST: KodePosController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, [Bind("ID,Kode,Kelurahan,Kecamatan,JenisWilayah,NamaKabupaten,Propinsi")] KodePosModel oParam)
        {
            try
            {
                if (id != oParam.ID)
                {
                    TempData["JScript"] = "Data Not Found";
                }



                //Delete to DB
                KodePosProvider.Delete(id);

                TempData["JScript"] = string.Format("Data {0} deleted", oParam.Kode);

                return Redirect("~/Home");
            }
            catch
            {
                return View();
            }
        }


        // GET: KodePosController/Print
        [Route("/KodePos/Print/{sKeyword?}")]
        public IActionResult Print(string? sKeyword)
        {

            if (CheckLogin() != null)
                return (ActionResult)CheckLogin();


            List<KodePosModel> oList = KodePosProvider.Get(sKeyword);

            string sPath = $"{AppDomain.CurrentDomain.BaseDirectory}" + @"Report\ReportKodePos.rdlc";
            Dictionary<string, string> rptParam = new Dictionary<string, string>();
            rptParam.Add("DateReport", DateTime.Now.ToString("dd-MM-yyyy"));

            AspNetCore.Reporting.LocalReport oReport = new AspNetCore.Reporting.LocalReport(sPath);
            oReport.AddDataSource("dsKodePos", oList);

            var oResult = oReport.Execute(RenderType.Excel, 1, rptParam, "");


            string filename = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "KodePos.xls");
            using (var fs = new FileStream(filename, FileMode.Create))
            {
                fs.Write(oResult.MainStream, 0, oResult.MainStream.Length);
                fs.Close();
            }

            string mimetype = "";
            int extension = 1;
            var result = oReport.Execute(RenderType.Excel, extension, null, mimetype);
            return File(result.MainStream, "application/msexcel", "KodePos.xls");
        }
    }
}
