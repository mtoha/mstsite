﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MstSite.Common;
using MstSite.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace MstSite.Controllers
{
    public class LoginController : BaseController
    {
        public static LoginModel UserInfoSession = null;
        public static bool CheckLogin()
        {
            bool bisLogin = false;

            if (LoginController.UserInfoSession != null && !string.IsNullOrWhiteSpace(LoginController.UserInfoSession.UserName))
            {
                bisLogin = true;
            }
            return bisLogin;
        }

        // GET: LoginController/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LoginController
        public ActionResult Index()
        {
            if (LoginController.UserInfoSession != null && !string.IsNullOrWhiteSpace(LoginController.UserInfoSession.UserName))
            {
                TempData["JScript"] = "Already Login " + LoginController.UserInfoSession.UserName;
                return Redirect("~/Home");
            }
            LoginModel oItem = new LoginModel();
            oItem.UserName = "mtoha2013";
            oItem.Password = "CorrectPassword";

            return View(oItem);
        }

        // POST: LoginController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("UserName,Password")] LoginModel oParam)
        {
            try
            {
                if (oParam.UserName.Equals("mtoha2013") && oParam.Password.Equals("CorrectPassword"))
                {
                    HttpContext.Session.SetString("UserInfoSession", JsonSerializer.Serialize(oParam));
                    LoginController.UserInfoSession = oParam;

                    SetNotif("Success Login. Welcome " + oParam.UserName);
                    return Redirect("~/Home");
                }
                else
                {
                    SetNotif("Wrong username or password ");
                    return Redirect("~/Home");
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: LoginController/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LoginController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: LoginController/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LoginController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}
