﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MstSite.Controllers
{
    public class BaseController : Controller
    {
        public void SetNotif(string sMessage) {
            TempData["JScript"] = sMessage;
        }

        public object CheckLogin() {

            if (!LoginController.CheckLogin())
            {
                SetNotif("You must Login first");

                return Redirect("~/Login");
            }
            return null;
        }
    }
}
