﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MstSite.Common;
using MstSite.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MstSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SatuanBarangController : ControllerBase
    {
        IWebHostEnvironment _env;
        public SatuanBarangController(IWebHostEnvironment env)
        {
            _env = env;
        }

        // GET: api/<SatuanBarangController>
        [HttpGet]
        public JsonResult Get()
        {
            string sQuery = "select * from dbo.SatuanBarang";
            DataTable dt = new DataTable();
            string sqlDataSource = CommonConstant.ConnectionStrings("MySales");
            SqlDataReader myReader;
            using (SqlConnection myConn = new SqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (SqlCommand myCommand = new SqlCommand(sQuery, myConn))
                {
                    myReader = myCommand.ExecuteReader();
                    dt.Load(myReader);
                    myReader.Close();
                    myConn.Close();
                }
            }


            return new JsonResult(dt);
        }

        // GET api/<SatuanBarangController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<SatuanBarangController>
        [HttpPost]
        public JsonResult Post(SatuanBarangModel oIn)
        {
            int iResult = -99;
            string sQuery = @"INSERT INTO MYSALES.dbo.SATUANBARANG
                                (NAME, LAMBANG, USER_CREATE, DATE_CREATE)
                                VALUES(@Name, @LAMBANG, @USER_CREATE, @DATE_CREATE);
                              Select @@identity";
            DataTable dt = new DataTable();
            string sqlDataSource = CommonConstant.ConnectionStrings("MySales");
            using (SqlConnection myConn = new SqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (SqlCommand myCommand = new SqlCommand(sQuery, myConn))
                {
                    myCommand.Parameters.AddWithValue("@Name", oIn.Name);
                    myCommand.Parameters.AddWithValue("@LAMBANG", oIn.Name);
                    myCommand.Parameters.AddWithValue("@USER_CREATE", oIn.Name);
                    myCommand.Parameters.AddWithValue("@DATE_CREATE", DateTime.Now);

                    iResult = Convert.ToInt32(myCommand.ExecuteScalar().ToString());
                    myConn.Close();
                }
            }


            return new JsonResult(iResult);
        }

        // PUT api/<SatuanBarangController>/5
        [HttpPut]
        public JsonResult Put(SatuanBarangModel oIn)
        {
            int iResult = -99;
            string sQuery = @"UPDATE dbo.SATUANBARANG
                                    SET NAME=@NAME, LAMBANG=@LAMBANG, USER_CREATE=@USER_CREATE 
                                    WHERE ID=@ID;
                                    ";
            DataTable dt = new DataTable();
            string sqlDataSource = CommonConstant.ConnectionStrings("MySales");
            using (SqlConnection myConn = new SqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (SqlCommand myCommand = new SqlCommand(sQuery, myConn))
                {
                    myCommand.Parameters.AddWithValue("@NAME", oIn.Name);
                    myCommand.Parameters.AddWithValue("@LAMBANG", oIn.Name);
                    myCommand.Parameters.AddWithValue("@USER_CREATE", oIn.Name);
                    myCommand.Parameters.AddWithValue("@ID", oIn.ID);

                    iResult = myCommand.ExecuteNonQuery();
                    myConn.Close();
                }
            }


            return new JsonResult(iResult);
        }

        // DELETE api/<SatuanBarangController>/5
        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            int iResult = -99;
            string sQuery = @"delete dbo.SATUANBARANG
                                    WHERE ID=@ID;
                                    ";
            DataTable dt = new DataTable();
            string sqlDataSource = CommonConstant.ConnectionStrings("MySales");
            using (SqlConnection myConn = new SqlConnection(sqlDataSource))
            {
                myConn.Open();
                using (SqlCommand myCommand = new SqlCommand(sQuery, myConn))
                {
                    myCommand.Parameters.AddWithValue("@ID", id);

                    iResult = myCommand.ExecuteNonQuery();
                    myConn.Close();
                }
            }


            return new JsonResult(iResult);
        }

        [Route("SaveFile")]
        [HttpPost]
        public JsonResult SaveFile()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                string sFileName = postedFile.FileName;
                var physicalPath = _env.ContentRootPath + "/Photos/" + sFileName;

                using (var stream = new FileStream(physicalPath, FileMode.Create))
                {
                    postedFile.CopyTo(stream);
                }
                return new JsonResult(sFileName);

            }
            catch (Exception ex)
            {
                return new JsonResult("anonymous.jpg");
            }
        }
    }
}
