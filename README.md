**MST Site**

Developed By MToha2013
https://stackoverflow.com/users/1084742/toha

We use : 
SQL Server 2012
.NET Core
C#
ASPNET Core MVC
jQuery Data Table
RDLC Reporting

---

## Restore Database

You’ll start by Restore Database


1. Clone Repository : git clone https://mtoha@bitbucket.org/mtoha/mstsite.git
2. Unzip .bak file : TesMST_db20211016.zip (on same folder at solution)
3. Open VS 2019 and open Solution : MstSite.sln
4. Configure appsettings.json and set **ConnectionString** value as your PC SQL Server 2012 instace and DB Name (built in config : "ConnectionString": "Server=DESKTOP-S0O222S\\SQL2012;Database=TesMST;User ID=sa;Password=Adm1nistrator")
5. Start project using IIS Express or You can attach project using IIS (built in on windows)
6. Login : mtoha2013 Password : CorrectPassword

---

## Requirements 

Requirements, you’ll need to prepare this

1. SQL Server 2012
2. IIS / IIS Express
3. VS 2019 
4. Internet Connection (to load CSS and JS file)
5. Browser
