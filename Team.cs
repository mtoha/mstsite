﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalbe.App.SakamorActivitySpreading.Api.Provider
{
    public static class HackerRankProvider
    {
        public static Dictionary<string, int> AverageAgeForEachCompany(List<Employee> employees)
        {
            var result = employees.GroupBy(s => s.Company)
                       .Select(g => new { Company = g.Key, Avg = g.Average(s => s.Age) }).OrderBy(x => x.Company)
                           .ToDictionary(x => x.Company, x => Convert.ToInt32(Math.Round(x.Avg, 0)));
            return result;

        }

        public static Dictionary<string, int> CountOfEmployeesForEachCompany(List<Employee> employees)
        {

            var result = employees.GroupBy(s => s.Company)
                       .Select(g => new { g.Key, Count = g.Count() }).OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Count);

            var x = new SortedDictionary<string, int>(result);
            var dict = new Dictionary<String, int>();
            foreach (var pair in x)
            {
                dict.Add(pair.Key, pair.Value);
            }

            return dict;
        }

        public static Dictionary<string, Employee> OldestAgeForEachCompany(List<Employee> employees)
        {
            var q = from s in employees
                    group s by s.Company into g
                    select new
                    {
                        Company = g.Key,
                        Employee = employees.Where(x => x.Company.Equals(x.Company) && x.Age == g.Max(s => s.Age)).FirstOrDefault()
                    };

            var result = q.OrderBy(x => x.Company).ToDictionary(x => x.Company, x => x.Employee);

            var x = new SortedDictionary<string, Employee>(result);
            var dict = new Dictionary<String, Employee>();
            foreach (var pair in x)
            {
                dict.Add(pair.Key, pair.Value);
            }

            return dict;

        }
    }
    public class Team
    {
        public string teamName { get; set; }
        public int noOfPlayers { get; set; }
        public Team(string TeamName, int NoOfPlayer)
        {
            teamName = TeamName;
            noOfPlayers = NoOfPlayer;
        }

        public int AddPlayer(int Count)
        {
            noOfPlayers = noOfPlayers + Count;
            return noOfPlayers;
        }

        public bool RemovePlayer(int count)
        {
            bool bIsSuccess = false;

            int iResult = noOfPlayers - count;
            if (iResult < 0)
                return bIsSuccess;
            else
                noOfPlayers = iResult;
            bIsSuccess = true;

            return bIsSuccess;
        }


    }


    public class Subteam : Team
    {
        public Subteam(string TeamName, int NoOfPlayer) : base(TeamName, NoOfPlayer)
        {
        }

        public void ChangeTeamName(string TeamName)
        {
            teamName = TeamName;
        }
    }

    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Company { get; set; }
    }
}
